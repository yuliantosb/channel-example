import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import {Chat} from 'react-native-super-app-channel';
import React from 'react';

const ChatScreen = ({navigation}) => {
  const onPressChannel = () => {
    navigation.navigate('ChannelDetails');
  };

  return (
    <SafeAreaView style={styles.container}>
      <Chat onPressChannel={onPressChannel} />
    </SafeAreaView>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
});
