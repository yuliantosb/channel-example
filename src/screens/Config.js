import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ConfigChannel, CreateChannel} from 'react-native-super-app-channel';

function Create({navigation}) {
  return (
    <SafeAreaView style={styles.container}>
      <ConfigChannel onNextPress={() => navigation.navigate('AddMember')} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default Create;
