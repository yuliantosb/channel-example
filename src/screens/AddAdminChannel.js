import {StyleSheet} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {AddAdmin} from 'react-native-super-app-channel';

function AddAdminChannel({navigation}) {
  return (
    <SafeAreaView style={styles.container}>
      <AddAdmin onCancel={() => navigation.goBack()} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default AddAdminChannel;
