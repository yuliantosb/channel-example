import * as React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {ListChannel} from 'react-native-super-app-channel';

export default function List({navigation}) {
  const data = [
    {
      id: 1,
      name: 'Moviestalk',
      message: 'Halo apa kabar',
      image: 'https://i.pravatar.cc/300?img=1',
      read: null,
      date: '14:00',
      badge: 2,
      type: 'receiver',
    },
    {
      id: 2,
      name: 'NewMovies',
      message: 'Hello...',
      image: 'https://i.pravatar.cc/300?img=2',
      read: false,
      date: 'Fri',
      badge: null,
      type: 'sender',
    },
    {
      id: 3,
      name: 'NetflixAddicts',
      message: 'Halo apa kabar',
      image: 'https://i.pravatar.cc/300?img=3',
      read: null,
      date: '14:00',
      badge: 2,
      type: 'receiver',
    },
    {
      id: 4,
      name: 'ActionMovies',
      message: 'ga sabar mau ntn',
      image: 'https://i.pravatar.cc/300?img=4',
      read: true,
      date: '14:00',
      badge: null,
      type: 'sender',
    },
    {
      id: 5,
      name: 'Billboardchart',
      message: 'Willy left the group',
      image: 'https://i.pravatar.cc/300?img=5',
      read: null,
      date: '21/07/2021',
      badge: null,
      type: 'receiver',
    },
    {
      id: 6,
      name: 'Nobar',
      message: 'yuk, abis bukber nobar guys',
      image: 'https://i.pravatar.cc/300?img=6',
      read: null,
      date: '21/07/2021',
      badge: 1,
      type: 'receiver',
    },
    {
      id: 7,
      name: 'Kpopmusic',
      message: 'BTS mau konser nih',
      image: 'https://i.pravatar.cc/300?img=7',
      read: false,
      date: '21/07/2021',
      badge: null,
      type: 'sender',
    },
    {
      id: 8,
      name: 'Moviestalk',
      message: 'thankyou...',
      image: 'https://i.pravatar.cc/300?img=5',
      read: true,
      date: '21/07/2021',
      badge: null,
      type: 'sender',
    },
    {
      id: 9,
      name: 'Nobar',
      message: 'yuk, abis bukber nobar guys',
      image: 'https://i.pravatar.cc/300?img=9',
      read: null,
      date: '21/07/2021',
      badge: 1,
      type: 'receiver',
    },
    {
      id: 10,
      name: 'Kpopmusic',
      message: 'BTS mau konser nih',
      image: 'https://i.pravatar.cc/300?img=10',
      read: false,
      date: '21/07/2021',
      badge: null,
      type: 'sender',
    },
    {
      id: 11,
      name: 'Moviestalk',
      message: 'thankyou...',
      image: 'https://i.pravatar.cc/300?img=11',
      read: true,
      date: '21/07/2021',
      badge: null,
      type: 'sender',
    },
    {
      id: 12,
      name: 'Moviestalk',
      message: 'Halo apa kabar',
      image: 'https://i.pravatar.cc/300?img=12',
      read: null,
      date: '14:00',
      badge: 2,
      type: 'receiver',
    },
    {
      id: 13,
      name: 'NewMovies',
      message: 'Hello...',
      image: 'https://i.pravatar.cc/300?img=13',
      read: false,
      date: 'Fri',
      badge: null,
      type: 'sender',
    },
    {
      id: 14,
      name: 'NetflixAddicts',
      message: 'Halo apa kabar',
      image: 'https://i.pravatar.cc/300?img=14',
      read: null,
      date: '14:00',
      badge: 2,
      type: 'receiver',
    },
    {
      id: 15,
      name: 'ActionMovies',
      message: 'ga sabar mau ntn',
      image: 'https://i.pravatar.cc/300?img=15',
      read: true,
      date: '14:00',
      badge: null,
      type: 'sender',
    },
  ];

  const handlePressAddGroup = () => {
    console.log('clicked');
    navigation.navigate('AllChannel');
  };

  const handlePressAddChannel = () => {
    navigation.navigate('OnBoardingChannel');
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ListChannel
          data={data}
          onAddGroupPress={handlePressAddGroup}
          onAddChannelPress={handlePressAddChannel}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    // paddingHorizontal: 20,
  },
});
