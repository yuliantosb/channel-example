import { StyleSheet } from 'react-native'
import React from 'react'
import { AllChannel } from 'react-native-super-app-channel'
import { SafeAreaView } from 'react-native-safe-area-context'

const All = () => {
  return (
    <SafeAreaView style={styles.container}>
      <AllChannel />
    </SafeAreaView>
  )
}

export default All

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
