import {StyleSheet} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Verification} from 'react-native-super-app-channel';

function VerificationLeave({navigation}) {
  return (
    <SafeAreaView style={styles.container}>
      <Verification onCancel={() => navigation.goBack()} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default VerificationLeave;
