import {StyleSheet} from 'react-native';
import React from 'react';
import {OnBoardingChannel} from 'react-native-super-app-channel';
import {SafeAreaView} from 'react-native-safe-area-context';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from 'src/App';

function Boarding({navigation}) {
  const handleAddChannel = () => {
    navigation.navigate('CreateChannel');
  };

  return (
    <SafeAreaView style={styles.container}>
      <OnBoardingChannel onAddPress={handleAddChannel} />
    </SafeAreaView>
  );
}

export default Boarding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
});
