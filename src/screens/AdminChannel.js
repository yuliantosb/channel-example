import {StyleSheet} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Administration} from 'react-native-super-app-channel';

function AdminChannel({navigation}) {
  return (
    <SafeAreaView style={styles.container}>
      <Administration
        onCancel={() => navigation.goBack()}
        onAddAdmin={() => navigation.navigate('AddAdmin')}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default AdminChannel;
