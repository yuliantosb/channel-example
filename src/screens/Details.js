import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import {ChannelDetails, Chat} from 'react-native-super-app-channel';
import React from 'react';

const Details = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ChannelDetails
        onSettingsPress={() => navigation.navigate('SettingsChannel')}
        onPassOwnership={() => navigation.navigate('Verification')}
        onAdmin={() => navigation.navigate('Admin')}
      />
    </SafeAreaView>
  );
};

export default Details;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
});
