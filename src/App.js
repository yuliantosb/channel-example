import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import List from './screens/List';
import All from './screens/All';
import Boarding from './screens/Boarding';
import Create from './screens/Create';
import Config from './screens/Config';
import Add from './screens/Add';
import ChatScreen from './screens/ChatScreen';
import Details from './screens/Details';
import Settings from './screens/Settings';
import VerificationLeave from './screens/VerificationLeave';
import AdminChannel from './screens/AdminChannel';
import AddAdminChannel from './screens/AddAdminChannel';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="ListChannel"
          component={List}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AllChannel"
          component={All}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="OnBoardingChannel"
          component={Boarding}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="CreateChannel"
          component={Create}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ConfigChannel"
          component={Config}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddMember"
          component={Add}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Chat"
          component={ChatScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="ChannelDetails"
          component={Details}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="SettingsChannel"
          component={Settings}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Verification"
          component={VerificationLeave}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Admin"
          component={AdminChannel}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="AddAdmin"
          component={AddAdminChannel}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
